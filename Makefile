# Makefile completo separando os diferentes elementos da aplicacao como codigo (src),
# cabecalhos (include), executaveis (build), bibliotecas (lib), etc.
# Cada elemento e alocado em uma pasta especifica, organizando melhor o codigo fonte.
#
# Algumas variaveis sao usadas com significado especial:
#
# $@ nome do alvo (target)
# $^ lista com os nomes de todos os pre-requisitos sem duplicatas
# $< nome do primeiro pre-requisito
#

# Comandos do sistema operacional
# Linux: rm -rf 
# Windows: cmd //C del 
RM = rm -rf 

# Compilador
CC=g++ 

# Variaveis para os subdiretorios
LIB_DIR=lib
INC_DIR=include
SRC_DIR=src
OBJ_DIR=build
BIN_DIR=bin
DOC_DIR=doc
TEST_DIR=test
 
# Opcoes de compilacao
CFLAGS=-Wall -pedantic -ansi -std=c++11 -I. -I$(INC_DIR)

# Garante que os alvos desta lista nao sejam confundidos com arquivos de mesmo nome
.PHONY: all init clean debug doxy doc

# Define o alvo (target) para a compilacao completa e os alvos de dependencia.
# Ao final da compilacao, remove os arquivos objeto.
all: init questao01 questao02 questao03 questao04 questao05 questao06

debug: CFLAGS += -g -O0
debug: all

# Alvo (target) para a criação da estrutura de diretorios
# necessaria para a geracao dos arquivos objeto 
init:
	@mkdir -p $(OBJ_DIR)/questao01
	@mkdir -p $(OBJ_DIR)/questao02
	@mkdir -p $(OBJ_DIR)/questao03
	@mkdir -p $(OBJ_DIR)/questao04
	@mkdir -p $(OBJ_DIR)/questao05
	@mkdir -p $(OBJ_DIR)/questao06

# Alvo (target) para a construcao do executavel sequencia (questao 01)
# Define os arquivos questao01/sequencia.o e questao01/calcsequencia.o como dependencias
questao01: CFLAGS+= -I$(INC_DIR)/questao01
questao01: $(OBJ_DIR)/questao01/sequencia.o $(OBJ_DIR)/questao01/calcsequencia.o
	@echo "============="
	@echo "Ligando o alvo $@"
	@echo "============="
	$(CC) $(CFLAGS) -o $(BIN_DIR)/sequencia $^
	@echo "+++ [Executavel sequencia criado em $(BIN_DIR)] +++"
	@echo "============="

# Alvo (target) para a construcao do objeto questao01/sequencia.o
# Define os arquivos questao01/sequencia.cpp e questao01/sequencia.h como dependencias.
$(OBJ_DIR)/questao01/sequencia.o: $(SRC_DIR)/questao01/sequencia.cpp $(INC_DIR)/questao01/sequencia.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo (target) para a construcao do objeto questao01/calcsequencia.o
# Define o arquivo questao01/calcsequencia.cpp como dependencia.
$(OBJ_DIR)/questao01/calcsequencia.o: $(SRC_DIR)/questao01/calcsequencia.cpp
	$(CC) -c $(CFLAGS) -o $@ $<


# Alvo (target) para a construcao do executavel mdc (questao 02)
# Define o arquivo questao02/mdc.o como dependencia.
questao02: CFLAGS+= -I$(INC_DIR)/questao02
questao02: $(OBJ_DIR)/questao02/mdc.o $(OBJ_DIR)/questao02/main.o
	@echo "============="
	@echo "Ligando o alvo $@"
	@echo "============="
	$(CC) $(CFLAGS) -o $(BIN_DIR)/mdc $^
	@echo "+++ [Executavel mdc criado em $(BIN_DIR)] +++"
	@echo "============="

# Alvo (target) para a construcao do objeto questao02/mdc.o
# Define o arquivo questao02/mdc.cpp como dependencia.
$(OBJ_DIR)/questao02/mdc.o: $(SRC_DIR)/questao02/mdc.cpp
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo (target) para a construcao do objeto questao02/main.o
# Define o arquivo questao02/main.cpp como dependencia.
$(OBJ_DIR)/questao02/main.o: $(SRC_DIR)/questao02/main.cpp
	$(CC) -c $(CFLAGS) -o $@ $<


# Alvo (target) para a construcao do executavel dec2bin (questao 03)
# Define o arquivo questao03dec2bin.o como dependencia.
questao03: CFLAGS+= -I$(INC_DIR)/questao03
questao03: $(OBJ_DIR)/questao03/dec2bin.o $(OBJ_DIR)/questao03/main.o
	@echo "============="
	@echo "Ligando o alvo $@"
	@echo "============="
	$(CC) $(CFLAGS) -o $(BIN_DIR)/dec2bin $^
	@echo "+++ [Executavel dec2bin criado em $(BIN_DIR)] +++"
	@echo "============="

# Alvo (target) para a construcao do objeto questao03/dec2bin.o
# Define o arquivo questao03/dec2bin.cpp como dependencia.
$(OBJ_DIR)/questao03/dec2bin.o: $(SRC_DIR)/questao03/dec2bin.cpp $(INC_DIR)/questao03/dec2bin.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo (target) para a construcao do objeto questao03/main.o
# Define o arquivo questao03/main.cpp como dependencia.
$(OBJ_DIR)/questao03/main.o: $(SRC_DIR)/questao03/main.cpp $(INC_DIR)/questao03/dec2bin.h
	$(CC) -c $(CFLAGS) -o $@ $<


# Alvo (target) para a construcao do executavel palindromo (questao 04)
# Define os arquivos questao04/tratastring.o e questao04/palindromo.o como dependencias
questao04: CFLAGS+= -I$(INC_DIR)/questao04
questao04: $(OBJ_DIR)/questao04/tratastring.o $(OBJ_DIR)/questao04/palindromo.o
	@echo "============="
	@echo "Ligando o alvo $@"
	@echo "============="
	$(CC) $(CFLAGS) -o $(BIN_DIR)/palindromo $^
	@echo "+++ [Executavel palindromo criado em $(BIN_DIR)] +++"
	@echo "============="

# Alvo (target) para a construcao do objeto questao04/tratastring.o
# Define os arquivos questao04/tratastring.cpp questao04/tratastring.h como dependencias.
$(OBJ_DIR)/questao04/tratastring.o: $(SRC_DIR)/questao04/tratastring.cpp $(INC_DIR)/questao04/tratastring.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo (target) para a construcao do objeto questao04/palindromo.o
# Define o arquivo questao04/main.cpp como dependencia.
$(OBJ_DIR)/questao04/palindromo.o: $(SRC_DIR)/questao04/palindromo.cpp
	$(CC) -c $(CFLAGS) -o $@ $<


# Alvo (target) para a construcao do executavel quadrado_recursivo (questao 05)
# Define o arquivo questao05/quadrado_recursivo.o como dependencia.
questao05: CFLAGS+= -I$(INC_DIR)/questao05
questao05: $(OBJ_DIR)/questao05/quadrado_recursivo.o $(OBJ_DIR)/questao05/quadrado_iterativo.o $(OBJ_DIR)/questao05/main_iterativo.o $(OBJ_DIR)/questao05/main_recursivo.o
	@echo "============="
	@echo "Ligando o alvo $@"
	@echo "============="
	$(CC) $(CFLAGS) -o $(BIN_DIR)/quadrado_recursivo $(OBJ_DIR)/questao05/quadrado_recursivo.o $(OBJ_DIR)/questao05/main_recursivo.o
	@echo "+++ [Executavel quadrado_recursivo criado em $(BIN_DIR)] +++"
	$(CC) $(CFLAGS) -o $(BIN_DIR)/quadrado_iterativo $(OBJ_DIR)/questao05/quadrado_iterativo.o $(OBJ_DIR)/questao05/main_iterativo.o
	@echo "+++ [Executavel quadrado_iterativo criado em $(BIN_DIR)] +++"
	@echo "============="

# Alvo (target) para a construcao do objeto questao05/quadrado_recursivo.o
# Define o arquivo questao05/quadrado_recursivo.cpp como dependencia.
$(OBJ_DIR)/questao05/quadrado_recursivo.o: $(SRC_DIR)/questao05/quadrado_recursivo.cpp
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo (target) para a construcao do objeto questao05/main_recursivo.o
# Define o arquivo questao05/main_recursivo.cpp como dependencia.
$(OBJ_DIR)/questao05/main_recursivo.o: $(SRC_DIR)/questao05/main_recursivo.cpp
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo (target) para a construcao do objeto questao05/quadrado_iterativo.o
# Define o arquivo questao05/quadrado_iterativo.cpp como dependencia.
$(OBJ_DIR)/questao05/quadrado_iterativo.o: $(SRC_DIR)/questao05/quadrado_iterativo.cpp
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo (target) para a construcao do objeto questao05/main_iterativo.o
# Define o arquivo questao05/main_iterativo.cpp como dependencia.
$(OBJ_DIR)/questao05/main_iterativo.o: $(SRC_DIR)/questao05/main_iterativo.cpp
	$(CC) -c $(CFLAGS) -o $@ $<


# Alvo (target) para a construcao do executavel ternaria (questao 06)
# Define os arquivos questao06/ternaria.o e questao06/busca.o como dependencias
questao06: CFLAGS+= -I$(INC_DIR)/questao06
questao06: $(OBJ_DIR)/questao06/ternaria.o $(OBJ_DIR)/questao06/busca.o
	@echo "============="
	@echo "Ligando o alvo $@"
	@echo "============="
	$(CC) $(CFLAGS) -o $(BIN_DIR)/ternaria $^
	@echo "+++ [Executavel ternaria criado em $(BIN_DIR)] +++"
	@echo "============="

# Alvo (target) para a construcao do objeto questao06/ternaria.o
# Define os arquivos questao06/ternaria.cpp e questao06/ternaria.h como dependencias.
$(OBJ_DIR)/questao06/ternaria.o: $(SRC_DIR)/questao06/ternaria.cpp $(INC_DIR)/questao06/ternaria.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo (target) para a construcao do objeto questao06/busca.o
# Define os arquitvos questao06/main.cpp como dependencia.
$(OBJ_DIR)/questao06/busca.o: $(SRC_DIR)/questao06/busca.cpp
	$(CC) -c $(CFLAGS) -o $@ $<



# Alvo (target) para a geração automatica de documentacao usando o Doxygen.
# Sempre remove a documentacao anterior (caso exista) e gera uma nova.
doxy:
	doxygen -g

doc:
	$(RM) $(DOC_DIR)/*
	doxygen

# Alvo (target) usado para limpar os arquivos temporarios (objeto)
# gerados durante a compilacao, assim como os arquivos binarios/executaveis.
clean:
	$(RM) $(BIN_DIR)/*
	$(RM) $(OBJ_DIR)/*

