#!/bin/bash
# Script para criacao de diretorios para estruturacao do projeto

mkdir -p bin
mkdir -p doc

# Diretorios base do repositorio
for dir in "build" "include" "src" "test";
do
	# Criacao do diretorio
	mkdir -p $dir
	
	# Criacao de subdiretorios
	for i in $(seq 1 6);
	do
		mkdir -p "$dir/questao0$i"
	done
done

