/**
 * @file	sequencia.h
 * @brief	Declaracao dos prototipos de funcoes que determinam o valor de sequencias
 *			com base no numero de termos recursiva e iterativa
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	17/03/2017
 * @date	18/03/2017
 */

#ifndef SEQUENCIA_H
#define SEQUENCIA_H


/**
 * @brief Funcao auxiliar que chama a respectiva funcao responsavel pelo 
 *		  calculo da sequencia com base nas entradas fornecidas pelo usuario
 * @param seq Identificador da sequencia
 * @param alg Tipo de algoritmo a ser utilizado para o calculo da sequencia
 * @param n Numero de termos a serem calculados
 */
void calcular_sequencia(char seq, char alg, int n);


/**
 * @brief Determina o valor da sequencia A = 1 + ... + 1/N de forma recursiva
 * @param n n-esimo termo da sequencia
 * @return Valor da sequencia
 */
double seqA_recursiva(int n);


/**
 * @brief Determina o valor da sequencia A = 1 + ... + 1/N de forma iterativa
 * @param n Numero de termos da sequencia
 * @return Valor da sequencia
 */
double seqA_iterativa(int n);


/**
 * @brief Determina o valor da sequencia B = 2/4 + ... + (N²+1)/(N+3) 
 * 		  de forma recursiva
 * @param n n-esimo termo da sequencia
 * @return Valor da sequencia
 */
double seqB_recursiva(int n);


/**
 * @brief Determina o valor da sequencia B = 2/4 + ... + (N²+1)/(N+3) 
 * 		  de forma iterativa
 * @param n Numero de termos da sequencia
 * @return Valor da sequencia
 */
double seqB_iterativa(int n);

#endif
