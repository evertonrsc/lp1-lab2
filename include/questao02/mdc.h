/**
 * @file	mdc.h
 * @brief	Declaracao dos prototipos de funcoes que permitem o calculo
 * 			do MDC
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	18/03/2017
 * @date	18/03/2017
 */

#ifndef MDC_H
#define MDC_H

/**
 * @brief Funcao recursiva que calcula o maximo divisor comum entre
 *		  dois numeros naturais positivos
 * @param a Dividendo
 * @param b Divisor
 * @return Maximo divisor comum entre os numeros
 */
int mdc(int a, int b);

#endif