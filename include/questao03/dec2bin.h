/**
 * @file	dec2bin.h
 * @brief	Declaracao dos prototipos de funcoes que permitem a conversão de
 * 			binário para decimal
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	18/03/2017
 * @date	18/03/2017
 */

#ifndef DEC2BIN_H
#define DEC2BIN_H

/**
 * @brief Funcao recursiva que converte um numero decimal para a sua 
		  forma binaria
 * @param n Numero decimal em questao 
 */
void dec2bin(int n);

#endif