/**
 * @file	quadrado.h
 * @brief	Declaracao dos prototipos de funcoes que permitem o calculo
 * 			do quadrado em suas versões recursiva e iterativa
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	18/03/2017
 * @date	18/03/2017
 */

#ifndef QUADRADO_H
#define QUADRADO_H

/**
 * @brief Funcao iterativa que calcula o quadrado de um numero inteiro
 * @param n Numero em questao
 */
int quadrado_iterativo(int n);

/**
 * @brief Funcao recursiva que calcula o quadrado de um numero inteiro
 * @param n Numero em questao
 */
int quadrado_recursivo(int n);

#endif