/**
 * @file	ternaria.h
 * @brief	Declaracao do prototipo da funcao que realiza uma busca ternaria
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author  Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	18/03/2017
 * @date	18/03/2017
 */

#ifndef TERNARIA_H
#define TERNARIA_H


/** 
 * @brief 	Funcao que implementa uma busca ternaria de forma recursiva fazendo 
 *			uso de limites inferior e superior para delimitar a parcela do vetor 
 *			sobre a qual a busca sera realizada
 * @details A busca ternaria recursiva divide o vetor sucessivamente em 
 * 			tres partes de mesmo tamanho (aproximado) e determina em 
 * 			qual dessas partes a chave a buscar se encontraria. A mesma
 *			funcao e chamada para realizar a busca nessa parte menor.
 *
 * @param 	v Vetor de inteiros sobre o qual a busca sera realizada
 * @param 	ini Limite inferior
 * @param	fim Limite superior
 * @param   x Chave de busca
 * @return 	Verdadeiro (true), caso o elemento exista no vetor,
 * 			falso (false) caso contrario
 */
bool busca_ternaria(int v[], int ini, int fim, int x);

#endif
