/**
 * @file	calcsequencia.cpp
 * @brief   Programa que determina o valor de sequencias com base no numero de 
 *			termos de forma recursiva e iterativa
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	17/03/2017
 * @date	21/03/2017
 */

#include <cstdlib>
using std::atoi;

#include <iostream>
using std::cerr;
using std::endl;

#include "sequencia.h"


/** 
 * @brief Funcao principal 
 * @param argc Numero de argumentos fornecidos via linha de comando
 * @param argv Argumentos fornecidos via linha de comando
 */
int main(int argc, char* argv[]) {
	if (argc != 4) {
		cerr << "Execucao incorreta do programa" << endl;
		cerr << "Modo de uso: ./sequencia <sequencia> <modo> <termos>" << endl;
		cerr << "\t<sequencia>: sequencia a ser calculada (A ou B)" << endl;
		cerr << "\t<modo>: algoritmo (R = recursivo, I = iterativo)" << endl;
		cerr << "\t<termos>: numero de termos a serem calculados" << endl;
		return EXIT_FAILURE;
	} else {
		char seq = (argv[1])[0];	// Sequencia a ser calculada
		char alg = (argv[2])[0];	// Algoritmo a ser utilizado para o calculo
		int n = atoi(argv[3]);		// Numero de termos a serem calculados

		if (seq != 'A' && seq != 'B') {
			cerr << "Sequencia inexistente." << endl;
			return EXIT_FAILURE;
		} else if (alg != 'R' && alg != 'I') {
			cerr << "Tipo de algoritmo incorreto." << endl;
			return EXIT_FAILURE;
		} else if (n <= 0) {
			cerr << "Numero de termos invalido." << endl;
			return EXIT_FAILURE;
		} 

		calcular_sequencia(seq, alg, n);
		return EXIT_SUCCESS;
	}
}
