/**
 * @file	sequencia.cpp
 * @brief	Implementacao de funcoes que determinam o valor de sequencias
 *			com base no numero de termos de forma recursiva e iterativa
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	17/03/2017
 * @date	18/03/2017
 * @sa		sequencia.h
 */

#include <cmath>
using std::pow;

#include <iostream>
using std::cout;
using std::endl;

#include <iomanip>
using std::setprecision;

#include <string>
using std::string;

#include "sequencia.h"


/**
 * @brief Funcao auxiliar que chama a respectiva funcao responsavel pelo 
 *		  calculo da sequencia com base nas entradas fornecidas pelo usuario
 * @param seq Identificador da sequencia
 * @param alg Tipo de algoritmo a ser utilizado para o calculo da sequencia
 * @param n Numero de termos a serem calculados
 */
void calcular_sequencia(char seq, char alg, int n) {
	double resultado = 0.0;
	string versao = (alg == 'R') ? "recursiva" : "iterativa";

	if (seq == 'A' && alg == 'R') {
		resultado = seqA_recursiva(n);
	} else if (seq == 'A' && alg == 'I') {
		resultado = seqA_iterativa(n);
	} else if (seq == 'B' && alg == 'R') {
		resultado = seqB_recursiva(n);
	} else if (seq == 'B' && alg == 'I') {
		resultado = seqB_iterativa(n);
	}

	cout << "O valor da sequencia " << seq << " para N = " << n << " e ";
	cout << setprecision(3) << resultado;
	cout << " (a versão " << versao << " foi usada)" << endl;
}


/**
 * @brief Determina o valor da sequencia A = 1 + ... + 1/N de forma recursiva
 * @param n n-esimo termo da sequencia
 * @return Valor da sequencia
 */
double seqA_recursiva(int n) {
	if (n == 1) {				// Caso base
		return 1.0;
	}

	// Caso recursivo
	return ((1.0 / n) + seqA_recursiva(n-1));
}


/**
 * @brief Determina o valor da sequencia A = 1 + ... + 1/N de forma iterativa
 * @param n Numero de termos da sequencia
 * @return Valor da sequencia
 */
double seqA_iterativa(int n) {
	double a = 0.0;
	for (int i = 1; i <= n; i++) {
		a += 1.0 / i;
	}
	return a;
}


/**
 * @brief Determina o valor da sequencia B = 2/4 + ... + (N²+1)/(N+3) 
 * 		  de forma recursiva
 * @param n n-esimo termo da sequencia
 * @return Valor da sequencia
 */
double seqB_recursiva(int n) {
	if (n == 1) {				// Caso base
		return 0.5;
	}

	// Caso recursivo
	return ((pow(n, 2) + 1.0) / (n + 3.0)) + seqB_recursiva(n-1);
}


/**
 * @brief Determina o valor da sequencia B = 2/4 + ... + (N²+1)/(N+3) 
 * 		  de forma iterativa
 * @param n Numero de termos da sequencia
 * @return Valor da sequencia
 */
double seqB_iterativa(int n) {
	double b = 0.0;
	for (int i = 1; i <= n; i++) {
		b += (pow(i, 2) + 1.0) / (i + 3.0);
	}
	return b;
}
