/**
 * @file	mdc.cpp
 * @brief	Programa que calcula recursivamente o maximo divisor comum (MDC) 
 *			entre dois numeros naturais positivos utilizando o algoritmo de 
 *			Euclides
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	18/03/2017
 * @date	18/03/2017
 */


#include <iostream>
#include "mdc.h"
using std::cin;
using std::cerr;
using std::cout;
using std::endl;

/** @brief Funcao principal */
int main() {
	int m, n;
	while(true) {
		cout << "Digite dois numeros naturais positivos: ";
		cin >> m >> n;

		if (m > 0 && n > 0) {
			break;
		} else {
			cerr << "Os numeros fornecidos como entrada sao invalidos. ";
			cerr << "Tente novamente." << endl;
		}
	}

	cout << "MDC(" << m << "," << n << ") = " << mdc(m, n) << endl;

	return 0;
}
