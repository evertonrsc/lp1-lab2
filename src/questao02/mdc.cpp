/**
 * @file	mdc.cpp
 * @brief	Programa que calcula recursivamente o maximo divisor comum (MDC) 
 *			entre dois numeros naturais positivos utilizando o algoritmo de 
 *			Euclides
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	18/03/2017
 * @date	18/03/2017
 */


#include <iostream>
#include "mdc.h"
using std::cin;
using std::cerr;
using std::cout;
using std::endl;


/**
 * @brief Funcao recursiva que calcula o maximo divisor comum entre
 *		  dois numeros naturais positivos
 * @param a Dividendo
 * @param b Divisor
 * @return Maximo divisor comum entre os numeros
 */
int mdc(int a, int b) {
	if (b == 0) {			// Caso base
		return a;
	}
	return mdc(b, a % b);	// Chamada recursiva
}
