/**
 * @file	dec2bin.cpp
 * @brief	Programa que converte um numero decimal para a sua forma binaria
 *			de forma recursiva
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	18/03/2017
 * @date	18/03/2017
 */


#include <iostream>
using std::cin;
using std::cerr;
using std::cout;
using std::endl;

#include "dec2bin.h"


/**
 * @brief Funcao recursiva que converte um numero decimal para a sua 
		  forma binaria
 * @param n Numero decimal em questao 
 */
void dec2bin(int n) {
	if (n <= 1) {		// Caso base
		cout << n;
		return;
	}

	dec2bin(n/2);		// Chamada recursiva
	cout << n % 2;
}