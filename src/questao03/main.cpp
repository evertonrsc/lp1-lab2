/**
 * @file	dec2bin.cpp
 * @brief	Programa que converte um numero decimal para a sua forma binaria
 *			de forma recursiva
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	18/03/2017
 * @date	18/03/2017
 */


#include <iostream>
using std::cin;
using std::cerr;
using std::cout;
using std::endl;

#include "dec2bin.h"


/** @brief Funcao principal */
int main() {
	int n;
	while (true) {
		cout << "Digite um numero: ";
		cin >> n;

		if (n > 0) {
			break;
		} else {
			cerr << "O numero a ser digitado deve ser positivo. ";
			cerr << "Tente novamente." << endl;
		}
	}

	cout << "Representacao de " << n << " na forma binaria: ";
	dec2bin(n);
	cout << endl;

	return 0;
}