/**
 * @file	palindromo.cpp
 * @brief	Programa que verifica de forma recursiva se uma palavra ou frase e 
 *			um palindromo
 * @details Um palindromo e uma palavra, frase ou qualquer outra sequencia de 
 *			unidades que tenha a propriedade de poder ser lida tanto da direita 
 *			para a esquerda como da esquerda para a direita. Num palindromo, 
 *			normalmente sao desconsiderados os sinais ortograficos, assim como 
 *			espacos entre palavras.
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author	Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	18/03/2017
 * @date	18/03/2017
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;

#include "tratastring.h"

/** @brief Funcao principal */
int main() {
	string s;
	cout << "Digite uma palavra ou frase: ";
	getline(cin, s);

	// Tratamento da string
	string ss = s;
	tratar_string(ss);
	
	// Verificacao se e palindromo
	if (isPalindromo(ss)) {
		cout << "\"" << s << "\" e um palindromo" << endl;
	} else {
		cout << "\"" << s << "\" nao e um palindromo" << endl;
	}

	return 0;
}
