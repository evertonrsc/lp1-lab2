/**
 * @file	main_recursivo.cpp
 * @brief	Programa que executa o calculo do quadrado de um numero utilizando uma recursao
 *			de cauda
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author  Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	18/03/2017
 * @date	18/03/2017
 */


#include <cstdlib>
using std::atoi;

#include <iostream>
using std::cerr;
using std::cout;
using std::endl;

#include "quadrado.h"

/**
 * @brief Funcao principal
 * @param argc Numero de argumentos recebidos via linha de comando
 * @param argv Argumentos recebidos via linha de comando
 */
int main(int argc, char* argv[]) {
	if (argc != 2) {
		cerr << "Execucao incorreta do programa" << endl;
		cerr << "Modo de uso: ./quadrado_iterativo <numero>" << endl;
		cerr << "\t<numero>: numero inteiro do qual sera calculado o quadrado" << endl;
		return EXIT_FAILURE;
	} else {
		int n = atoi(argv[1]);
		cout << "quadrado(" << n << ") => " << quadrado_recursivo(n) << endl;
		return EXIT_SUCCESS;
	}
}
