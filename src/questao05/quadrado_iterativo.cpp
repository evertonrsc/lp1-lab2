/**
 * @file	quadrado_iterativo.cpp
 * @brief	Programa que calcula o quadrado de um numero de forma iterativa
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author  Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	18/03/2017
 * @date	18/03/2017
 */


#include <cstdlib>
using std::atoi;

#include <iostream>
using std::cerr;
using std::cout;
using std::endl;

#include "quadrado.h"

/**
 * @brief Funcao iterativa que calcula o quadrado de um numero inteiro
 * @param n Numero em questao
 */
int quadrado_iterativo(int n) {
	int n2 = 0;
	int i = 1;
	while (i <= n) {		// Condicao de parada
		n2 += (2*i)-1;		// Computacao de resultado
		cout << (2*i)-1;
		i++;				// Ajuste de parametro
		if (i != n+1) {
			cout << " + ";
		}
	}

	return n2;
}

