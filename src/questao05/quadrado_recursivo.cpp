/**
 * @file	quadrado_recursivo.cpp
 * @brief	Programa que calcula o quadrado de um numero utilizando uma recursao
 *			de cauda
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author  Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	18/03/2017
 * @date	18/03/2017
 */


#include <cstdlib>
using std::atoi;

#include <iostream>
using std::cerr;
using std::cout;
using std::endl;

#include "quadrado.h"

/**
 * @brief Funcao recursiva que calcula o quadrado de um numero inteiro
 * @param n Numero em questao
 */
int quadrado_recursivo(int n) {
	if (n <= 1) {						// Caso base
		return n;
	}
	return (2*n)-1 + quadrado_recursivo(n-1);		// Chamada recursiva
}

