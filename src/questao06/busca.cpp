/**
 * @file	busca.cpp
 * @brief	Programa que realiza uma busca por uma chave em um vetor de inteiros
 *			utilizando uma busca ternaria recursiva
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author  Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	18/03/2017
 * @date	18/03/2017
 */

#include <cstdlib>
using std::atoi;

#include <iostream>
using std::cerr;
using std::cout;
using std::endl;

#include "ternaria.h"


/**
 * @brief Funcao principal
 * @param argc Numero de argumentos fornecidos via linha de comando
 * @param argv Argumentos fornecidos via linha de comando
 */
int main(int argc, char* argv[]) {
	if (argc != 2) {
		cerr << "Execucao incorreta do programa" << endl;
		cerr << "Modo de uso: ./ternaria <chave>" << endl;
		cerr << "\t<chave>: valor inteiro a ser buscado no vetor" << endl;
		return EXIT_FAILURE;
	} else {
		int chave = atoi(argv[1]);			// Chave a ser buscada

		// Vetor de inteiros sobre o qual o 
		const int tam = 25;
		int vetor[] = { 2, 5, 9, 11, 13, 17, 22, 24, 33, 38, 39, 40, 45, 
			56, 71, 99, 110, 113, 132, 155, 166, 203, 211, 212, 230, 233 };

		// Exibicao do resultado da busca
		if (busca_ternaria(vetor, 0, tam-1, chave)) {
			cout << "O elemento " << chave << " faz parte do vetor." << endl;
		} else {
			cout << "O elemento " << chave << " nao faz parte do vetor." << endl;
		}

		return EXIT_SUCCESS;
	}
}
