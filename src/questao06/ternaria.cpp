/**
 * @file	ternaria.cpp
 * @brief	Implementacao da funcao que realiza uma busca ternaria
 * @author	Everton Cavalcante (everton@dimap.ufrn.br)
 * @author  Silvio Sampaio (silviocs@imd.ufrn.br)
 * @since	18/03/2017
 * @date	18/03/2017
 * @sa		ternaria.h
 */


#include <cmath>
using std::floor;

#include "ternaria.h"


/** 
 * @brief 	Funcao que implementa uma busca ternaria de forma recursiva fazendo 
 *			uso de limites inferior e superior para delimitar a parcela do vetor 
 *			sobre a qual a busca sera realizada
 * @details A busca ternaria recursiva divide o vetor sucessivamente em 
 * 			tres partes de mesmo tamanho (aproximado) e determina em 
 * 			qual dessas partes a chave a buscar se encontraria. A mesma
 *			funcao e chamada para realizar a busca nessa parte menor.
 *
 * @param 	v Vetor de inteiros sobre o qual a busca sera realizada
 * @param 	ini Limite inferior
 * @param	fim Limite superior
 * @param   x Chave de busca
 * @return 	Verdadeiro (true), caso o elemento exista no vetor,
 * 			falso (false) caso contrario
 */
bool busca_ternaria(int v[], int ini, int fim, int x) {
	if (ini <= fim) {
		int t1 = floor(ini + (fim-ini) / 3);		// Primeiro limite
		int t2 = floor(ini + 2 * (fim-ini) / 3);	// Segundo limite

		if (v[t1] == x) {
			return true;						// Chave encontrada no primeiro limite
		} else if (v[t2] == x) {
			return true;						// Chave encontrada no segundo limite
		} else if (v[t1] > x) {
			return busca_ternaria(v, ini, t1-1, x);		// Busca no primeiro terco
		} else if (v[t2] < x) {
			return busca_ternaria(v, t2+1, fim, x);		// Busca no terceiro terco
		} else {
			return busca_ternaria(v, t1+1, t2-1, x);	// Busca no segundo terco
		}
	}

	return false;								// Chave nao encontrada
}
